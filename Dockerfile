FROM node:latest

# Get pandoc
RUN wget https://github.com/jgm/pandoc/releases/download/2.9.2.1/pandoc-2.9.2.1-1-amd64.deb
RUN dpkg -i pandoc-2.9.2.1-1-amd64.deb

RUN mkdir /app
WORKDIR /app
COPY . /app
RUN npm install
RUN mkdir /confluence_html
RUN mkdir /markdown

ENTRYPOINT ["npm", "run"]
CMD ["start"]
